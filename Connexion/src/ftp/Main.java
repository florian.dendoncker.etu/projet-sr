package ftp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {

    private static PrintWriter printWriter;
    private static BufferedReader bufferedReader;


    /* Fonction principale du programme executant dans l'ordre les differentes fonctions necessaires a l'affichage des
    fichiers et dossiers du dossier source auquel on se connecte
    args :
    args[0] : l'adresse ip du serveur FTP
    args[1] : le nom d'utilisateur de l'utilisateur
    args[2] : le mot de passe de l'utilisateur
    */
    public static void main(String[] args){

        /* On commence par creer un utilisateur */
        Utilisateur utilisateur = new Utilisateur(args[1], args[2]);

        /* On initialise les commandes que l'on peut envoyer au serveur */
        Commandes commandes = new Commandes(utilisateur.getNom(),utilisateur.getMotDePasse());

        /* Puis on initialise l'adresse et le port, qui serviront a la connexion. */
        /* L'adresse est donnée en paramètre de la fonction main. */
        /* Le port est toujours 21 */
        String adresse = args[0];
        int port = 21;



        /* On se connecte au serveur */
        try {

            /* On ouvre le socket de connexion */
            Socket socket = new Socket(adresse, port);
            printWriter = new PrintWriter(socket.getOutputStream(),true);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));


            /* On envoie les messages necessaires a la connexion et on print leur reponse */
            System.out.println(bufferedReader.readLine());
            System.out.println(("USER"));
            System.out.println(envoiReception(commandes.user()));

            /*
            On envoie el mot de passe de l'utilisateur au serveur
            Renvoie une erreur "User can't log-in"  et empeche la suite du programme de s'executer correctement
             */
            System.out.println(("PASS"));
            System.out.println(envoiReception(commandes.pass()));


            /* Affichage du dossier source */
            System.out.println(("PWD"));
            System.out.println(envoiReception(commandes.pwd()));


            /* On ouvre un socket en mode passif (necessaire pour l'affichage des informations en arbre) */
            System.out.println(("PASV"));
            String infos_socket_passif = envoiReception(commandes.pasv());
            System.out.print(infos_socket_passif);
            Socket socket_passif = new Socket(get_adress(infos_socket_passif), get_port(infos_socket_passif));
            printWriter = new PrintWriter(socket_passif.getOutputStream(),true);
            bufferedReader = new BufferedReader(new InputStreamReader(socket_passif.getInputStream()));

            /* Affichage des dossiers contenus dans le dossier source */
            System.out.println(("LIST"));
            System.out.println(envoiReception(commandes.list()));



            /* On ferme les sockets */
            socket.close();
            socket_passif.close();
        }


        catch(IOException e) {
            System.out.println( "Erreur I/O.");
        }

    }

    public static String get_adress(String infos){
        String[] temp =  infos.substring(infos.indexOf('(')+1,infos.indexOf(')')).split(",");
        return temp[0]+'.'+temp[1]+'.'+temp[2]+'.'+temp[3];
    }

    public static int get_port(String infos){
        String[] temp =  infos.substring(infos.indexOf('(')+1,infos.indexOf(')')).split(",");
        return (Integer.parseInt(temp[4])*256) + Integer.parseInt(temp[5]);
    }


    /* Envoie les commandes au serveur FTP et permet l'affichage des reponses du serveur dans la console
       Args :
       commande : la commande a executer sur le serveur FTP
    */
    public static String envoiReception(String commande) throws java.io.IOException{
        printWriter.println(commande);
        return bufferedReader.readLine();
    }
}
