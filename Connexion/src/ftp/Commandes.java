package ftp;

/* Definit les differentes commandes utilisables sur un serveur FTP */
public class Commandes {
    
    private String nom;
    private String motDePasse;

    // Connexion : nom utilisateur
    public String user() {
        return ("USER " + this.nom + "\r");
    }

    // Connexion : mot de passe
    public String pass() {
        return ("PASS " + this.motDePasse + "\r");
    }

    // Connexion via l'authentification TLS (non utilisée)
    public String authTLS() {
        return "AUTH TLS " + "\r";
    }

    // Connexion via l'authentification SSL (non utilisée)
    public String authSSL() {
        return "AUTH SSL " + "\r";
    }

    // Affichage du dossier courant dans lequel l'utilisateur navigue
    public String pwd() {
        return "PWD \r";
    }

    // Affichage de la liste de fichiers et dossiers contenus dans le dossier courant.
    public String list() {
        return "LIST \r";
    }

    // Changement de dossier courant
    public String cwd() {
        return "CWD \r";
    }

    // Passage du socket en mode passif (necessite d'ensuite redefinir un socket avec les informations recues)
    public String pasv() {
        return "PASV \r";
    }
    
    public Commandes(String nom,String motDePasse){
        this.nom = nom;
        this.motDePasse = motDePasse;
    }
        
}
