package ftp;


// Definit un utilisateur par son identifiant et son mot de passe.
public class Utilisateur {
    private String nom;
    private String motDePasse;

    public String getNom() {
        return nom;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public Utilisateur(String nom, String motDePasse){
        this.nom = nom;
        this.motDePasse = motDePasse;
    }
}
