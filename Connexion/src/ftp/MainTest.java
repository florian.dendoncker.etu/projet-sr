package ftp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest extends Main {

    @Test
    void testGet_adress() {
        String infos = "reponse = (127,0,0,1,255,128)";
        assertEquals("127.0.0.1",get_adress(infos));
    }

    @Test
    void testGet_port() {
        String infos = "reponse = (127,0,0,1,255,128)";
        assertEquals(65408,get_port(infos));
    }
}