# Projet SR

## 1 Concept et avancée

L'idée de ce TP est de mettre en place en java un outil de connexion a un serveur FTP, et de lui faire afficher une arborescence récursive du contenu du serveur.



Malheureusement pour une raison inconnue, le projet est resté bloqué sur un message d'erreur lors de la tentative de connexion de l'utilisateur. Les identifiants utilisés sont les mêmes que lors d'une tentative de connexion utilisant Filezilla, les messages envoyés sont strictement les mêmes, mais une erreur "User can't log in" empêche la connexion via l'outil développé. Ce problème n'apparaissait pas au début de ce projet et les commande envoyées n'ont pas été modifiées, la raison de ce problème reste inconnue.



## 2 Fonctionnement

Le projet, dans son état actuel, se divise en 3 classes : 

**Utilisateur** : Définit simplement les identifiants de l'utilisateur qui se connecte au serveur. Utilisé pour générer les commandes de connexion dans la classe Commandes

**Commandes** : Définit les différentes commandes que l'on peut envoyer au serveur FTP, telles que les commandes de connexion, l'affichage d'une liste de dossiers, etc etc...

Compose certaines commandes avec des informations externes, par exemple, les commandes de connexions iront chercher le nom d'utilisateur et le mot de passe entrés en paramètres au lancement du programme.

**Main** : Classe principale lançant dans l'ordre les différentes commandes nécessaires a l'affichage des fichiers / dossiers, cette classe rencontre pour l'instant une erreur "User can't log-in" lors de l'envoi du mot de passe de l'utilisateur, erreur qui semble indiquer un problème de droits d'accès mais qui pourtant n'apparait pas lors d'une connexion en utilisant Filezilla.



## 3 Utilisation

Pour exécuter le programme, il faut lancer la fonction **main** de la classe **Main** avec comme arguments, dans l'ordre : 

1. l'adresse IP du serveur
2. le nom d'utilisateur
3. le mot de passe

le port de connexion est prédéfini dans le programme comme étant le port 21 et ne fait donc pas parti des arguments de lancement.





